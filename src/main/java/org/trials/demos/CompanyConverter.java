package org.trials.demos;



import org.bson.types.ObjectId;



import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;

public class CompanyConverter {

	// convert Person Object to MongoDB DBObject
	// take special note of converting id String to ObjectId
	public static DBObject toDBObject(Company c) {

		BasicDBObjectBuilder builder = BasicDBObjectBuilder.start()
				.append("companyid", c.getCompanyid()).append("companyname", c.getCompanyname());
		if (c.getId() != null)
			builder = builder.append("_id", new ObjectId(c.getId()));
		return builder.get();
	}

	// convert DBObject Object to Person
	// take special note of converting ObjectId to String
	public static Company toCompany(DBObject doc) {
		Company c = new Company();
		c.setCompanyid((String) doc.get("commpanyid"));
		c.setCompanyname((String) doc.get("companyname"));
		ObjectId id = (ObjectId) doc.get("_id");
		c.setId(id.toString());
		return c;

	}
	
}
