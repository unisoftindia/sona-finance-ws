package org.trials.demos;

public class Company {
	private String id;
 private String companyid;
 private String companyname;
 private String companyaddress;
 private String companyaddress1;
 private String companycity;
 private int pincode;
 private String companyemail;
 private int contactnumber;
 private String companytin;
 private String vattin;
 private String csttin;
 private String panno;
 private String eccno;
 
public String getCompanyid() {
	return companyid;
}
public void setCompanyid(String companyid) {
	this.companyid = companyid;
}
public String getCompanyname() {
	return companyname;
}
public void setCompanyname(String companyname) {
	this.companyname = companyname;
}
public String getCompanyaddress() {
	return companyaddress;
}
public void setCompanyaddress(String companyaddress) {
	this.companyaddress = companyaddress;
}
public String getCompanyaddress1() {
	return companyaddress1;
}
public void setCompanyaddress1(String companyaddress1) {
	this.companyaddress1 = companyaddress1;
}
public String getCompanycity() {
	return companycity;
}
public void setCompanycity(String companycity) {
	this.companycity = companycity;
}
public int getPincode() {
	return pincode;
}
public void setPincode(int pincode) {
	this.pincode = pincode;
}
public String getCompanyemail() {
	return companyemail;
}
public void setCompanyemail(String companyemail) {
	this.companyemail = companyemail;
}
public int getContactnumber() {
	return contactnumber;
}
public void setContactnumber(int contactnumber) {
	this.contactnumber = contactnumber;
}
public String getCompanytin() {
	return companytin;
}
public void setCompanytin(String companytin) {
	this.companytin = companytin;
}
public String getVattin() {
	return vattin;
}
public void setVattin(String vattin) {
	this.vattin = vattin;
}
public String getCsttin() {
	return csttin;
}
public void setCsttin(String csttin) {
	this.csttin = csttin;
}
public String getPanno() {
	return panno;
}
public void setPanno(String panno) {
	this.panno = panno;
}
public String getEccno() {
	return eccno;
}
public void setEccno(String eccno) {
	this.eccno = eccno;
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
} 
}