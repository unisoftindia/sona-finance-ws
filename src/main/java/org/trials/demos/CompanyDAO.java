package org.trials.demos;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;



import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


//DAO class for different MongoDB CRUD operations
//take special note of "id" String to ObjectId conversion and vice versa
//also take note of "_id" key for primary key
public class CompanyDAO {

    public static final String MONGODB_NAME = "FinanceTrial";
    public static final String MONGODB_COMPANY_COLLECTION = "CompanyTrial";
    public static final String MONGODB_OBJECTID = "_id";
    
	private DBCollection col;

	public CompanyDAO(MongoClient mongo) {
		this.col = mongo.getDB(MONGODB_NAME).getCollection();
	}

	public Company createCompany(Company c) {
		DBObject doc = CompanyConverter.toDBObject(c);
		Object id = this.col.insert(doc).getField(MONGODB_OBJECTID) ;
		//ObjectId id = (ObjectId) doc.get(MONGODB_OBJECTID);
		c.setId(id.toString());
		return c;
	}

	public void updateCompany(Company c) {
		DBObject query = BasicDBObjectBuilder.start()
				.append(MONGODB_OBJECTID, new ObjectId(c.getId())).get();
		this.col.update(query, CompanyConverter.toDBObject(c));
	}

	public List<Company> readAllCompany() {
		List<Company> data = new ArrayList<Company>();
		DBCursor cursor = col.find();
		while (cursor.hasNext()) {
			DBObject doc = cursor.next();
			Company c= CompanyConverter.toCompany(doc);
			
			data.add(c);
		}
		return data;
	}

	public void deleteCompany(Company c) {
		DBObject query = BasicDBObjectBuilder.start()
				.append(MONGODB_OBJECTID, new ObjectId(c.getId())).get();
		this.col.remove(query);
	}

	public Company readAllCompany(Company c) {
		DBObject query = BasicDBObjectBuilder.start()
				.append(MONGODB_OBJECTID, new ObjectId(c.getId())).get();
		DBObject data = this.col.findOne(query);
		return CompanyConverter.toCompany(data);
	}

}