package org.trials.demos;

import java.net.UnknownHostException;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;


/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "Got it!";
    }
    
    @POST  
    @Path("addCompany")  
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCompany(  
      
            private static final String COMPANY_ID = "companyid";
        
    		  @FormParam() String companyid,
    		  @FormParam("companyname") String companyname,
    		  @FormParam("companyaddress") String companyaddress,
    		  @FormParam("companyaddress1") String companyaddress1,
    		  @FormParam("companycity") String companycity,
    		  @FormParam("pincode") int pincode,
    		  @FormParam("companyemail") String companyemail,
    		  @FormParam("contactnumber") int contactnumber,
    		  @FormParam("companytin") String companytin,
    		  @FormParam("vattin") String vattin,
    		  @FormParam("csttin") String csttin,
    		  @FormParam("panno") String panno,
    		  @FormParam("eccno") String eccno 
    		  
   ) throws UnknownHostException {  
    	MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("Finance"); 
        DBCollection col = db.getCollection("Company");
        BasicDBObjectBuilder document = BasicDBObjectBuilder.start();
     
        document.append("companyid", companyid);
        document.append("companyname", companyname);
        document.append("companyaddress", companyaddress);
        document.append("companyaddress1", companyaddress1);
        document.append("companycity", companycity);
        document.append("pincode", pincode);
        document.append("companyemail", companyemail);
        document.append("contactnumber", contactnumber);
        document.append("companytin", companytin);
        document.append("vattin", vattin);
        document.append("csttin", csttin);
        document.append("panno", panno);
        document.append("eccno", eccno);
        col.insert(document.get());  
    	return Response.status(200).entity("Add to database")  .build();
    
    }
    
    @GET  
    @Path("getCompany")  
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCompany () throws UnknownHostException
    {
    	 MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
         DB db = mongoClient.getDB("Finance");
         DBCollection collection = db.getCollection("Company");
        
         DBCursor cursor = collection.find();
         DBObject object = cursor.next();
         
         if (object == null) {
             throw new WebApplicationException(Response.Status.NOT_FOUND);
         }
         return Response.ok(object.toString()).build();  
	}
		
    

    @POST  
    @Path("addDemo")  
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addDemo(  
      
    		  @FormParam("name") String name,
    		  @FormParam("email") String email,
    		  @FormParam("favc") String favc)
    {
    return Response.status(200).entity("Add to database")  .build();
}

    
}